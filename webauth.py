from flask import Flask, request, jsonify
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import timedelta

webauth = Flask(__name__)
webauth.config['JWT_SECRET_KEY'] = 'yPKijNM3xh6$rcxvjZ*wLb'
webauth.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(minutes=0)
jwt = JWTManager(webauth)

limiter = Limiter(
    get_remote_address,
    app=webauth,
    storage_uri="memory://",
)

# stockage des utilisateurs
users = {
    'admin': {
        'password': generate_password_hash('admin123'),
        'roles': ['admin']
    }
}

# Route pour créer un compte utilisateur
@webauth.route('/create_user', methods=['POST'])
def create_user():
    if not request.is_json:
        return jsonify({"msg": "Les donnees doivent etre au format JSON"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username or not password:
        return jsonify({"msg": "Nom d'utilisateur et mot de passe requis"}), 400

    if username in users:
        return jsonify({"msg": "Nom d'utilisateur deja existant"}), 400

    users[username] = {
        'password': generate_password_hash(password),
        'roles': ['user']
    }

    return jsonify({"msg": "Compte utilisateur cree avec succes"}), 201

# Route pour obtenir un token
@webauth.route('/get_token', methods=['POST'])
@limiter.limit("5 per minute")
def get_token():
    if not request.is_json:
        return jsonify({"msg": "Les donnees doivent etre au format JSON"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username or not password:
        return jsonify({"msg": "Nom d'utilisateur et mot de passe requis"}), 400

    if username not in users or not check_password_hash(users[username]['password'], password):
        return jsonify({"msg": "Nom d'utilisateur ou mot de passe incorrect"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token), 200

# Route pour afficher/éditer un compte utilisateur
@webauth.route('/user_profile', methods=['GET', 'PUT'])
@limiter.limit("5 per minute")
@jwt_required()
def user_profile():
    current_user = get_jwt_identity()
    user = users.get(current_user)

    if request.method == 'GET':
        user_data = {
            "username": current_user,
            "roles": user.get('roles', [])
        }
        return jsonify(user_data), 200

    elif request.method == 'PUT':
        new_username = request.json.get('new_username', None)
        new_password = request.json.get('new_password', None)
        if new_username or new_password:

            if new_username != current_user and new_username in users:
                return jsonify({"msg": "Le nouvel utilisateur existe déjà"}), 400

            if new_password:
                user['password'] = generate_password_hash(new_password)

            if new_username:
                user_data = {
                    'password': user['password'],
                    'roles': user.get('roles', [])
                }
                users[new_username] = user_data
                del users[current_user]

        return jsonify({"msg": "Compte utilisateur edite avec succes"}), 200

if __name__ == '__main__':
    webauth.run(debug=True)
