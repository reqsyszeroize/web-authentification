- # WebAuth

Cette API permet l'authentification d'utilisateurs. Elle permet la création de comptes, l'affichage & l'édition de ces derniers ainsi que la création de jetons renouvelables.
Cette application comprend des modules de protections contre les tentatives de bruteforce.

## Installation et exécution

- Assurez-vous d'avoir Python installé.
- Création de l'environnement virtuel via `python3 -m venv venv && source venv/bin/activate`
- Installation des dépendances via `pip install Flask Flask-JWT-Extended Flask-Limiter Flask_Login`.
- Execution de l'application via `python3 webauth.py`.

## Routes

### Création d'un utilisateur 

- URL : `/create_user`
- Méthode : `POST`
- Description : Cette route permet la création d'un nouvel utilisateur.

- Exemple :  
```shell
curl -X POST -H "Content-Type: application/json" -d '{"username": "toto", "password": "toto123"}' http://localhost:5000/create_user
```

### Récupération / Renouvellement du token utilisateur

- URL : `/get_token`
- Méthode : `POST`
- Description : Cette route permet la récupération / le renouvellement du token.

- Exemple :  
```shell
curl -X POST -H "Content-Type: application/json" -d '{"username": "toto", "password": "toto123"}' http://localhost:5000/get_token
```

### Affichage d'un compte utilisateur

- URL : `/user_profile`
- Méthode : `GET`
- Description : Cette route permet d'afficher les informations relatives au compte utilisateur via son token.

- Exemple :  
```shell
curl -X GET -H "Authorization: Bearer <<TOKEN_UTILISATEUR>>" http://localhost:5000/user_profile
```

### Edition d'un compte utilisateur

- URL : `/user_profile`
- Méthode : `PUT`
- Description : Cette route permet de modifier les informations relatives au compte utilisateur via son token.

- Exemple :  
```shell
curl -X PUT -H "Authorization: Bearer <<TOKEN_UTILISATEUR>>" -H "Content-Type: application/json" -d '{"new_username": "toto2", "new_password": "toto321"}' http://localhost:5000/user_profile
```
